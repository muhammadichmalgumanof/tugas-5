package com.ichmal.aplikasi_struktur_organisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiStrukturOrganisasiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AplikasiStrukturOrganisasiApplication.class, args);
    }

}
