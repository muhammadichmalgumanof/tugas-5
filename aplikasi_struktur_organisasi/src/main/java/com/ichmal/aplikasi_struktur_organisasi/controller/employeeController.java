package com.ichmal.aplikasi_struktur_organisasi.controller;

import com.ichmal.aplikasi_struktur_organisasi.model.employee;
import com.ichmal.aplikasi_struktur_organisasi.service.employeeService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;
import java.util.List;

@Controller
public class employeeController {

    @Autowired
    private employeeService empService;

    @RequestMapping("/employee")
    public String viewHomePage(Model model){
        List<employee> listEmployee = empService.listTampil();
        model.addAttribute("listEmployee", listEmployee);
        return "employee";
    }

    @RequestMapping("/employee/add")
    public String viewPageAddEmployee(Model model){
        employee emp = new employee();
        model.addAttribute(emp);
        return "employee_create";
    }

    @RequestMapping(value = "/saveEmployee", method = RequestMethod.POST)
    public String addEmployee(@ModelAttribute("employee") employee model){
        empService.save(model);
        return("redirect:/employee");
    }

    @RequestMapping("/employee/edit/{id}")
    public ModelAndView viewEditEmployee(@PathVariable(name = "id") int id){
        ModelAndView mav = new ModelAndView("employee_edit");
        employee emp = empService.get(id);
        mav.addObject("emp", emp);
        return mav;
    }

    @RequestMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable(name = "id") int id){
        empService.delete(id);
        return "redirect:/employee";
    }

    @RequestMapping("/createReportPdfEmployee")
    public String reportsPdf() throws FileNotFoundException, JRException {
        empService.exportReport("pdf");
        return "redirect:/employee";
    }

    @RequestMapping ("/createReportXlsEmployee")
    public String reportXls() throws FileNotFoundException, JRException{
        empService.exportReport("xlsx");
        return "redirect:/employee";
    }
}
