package com.ichmal.aplikasi_struktur_organisasi.controller;

import com.ichmal.aplikasi_struktur_organisasi.model.company;
import com.ichmal.aplikasi_struktur_organisasi.service.companyService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class companyController {
    @Autowired
    private companyService cmpService;

    @RequestMapping("/company")
    public String viewHomePageCompany(Model model){
        List<company> listCompany = cmpService.listAll();
        model.addAttribute("listCompany", listCompany);
        return "company";
    }

    @RequestMapping("/company/add")
    public String addCompanyPage(Model model){
        company cmp = new company();
        model.addAttribute(cmp);
        return "company_create";
    }

    @RequestMapping(value = "/saveCompany", method = RequestMethod.POST)
    public String addCompany(@ModelAttribute("company") company model){
        cmpService.save(model);
        return "redirect:/company";
    }

    @RequestMapping("/company/edit/{id}")
    public ModelAndView viewUpdateCompany(@PathVariable(name = "id") int id){
        ModelAndView mav = new ModelAndView("company_edit");
        company cmp = cmpService.getId(id);
        mav.addObject("cmp", cmp);
        return mav;
    }

    @RequestMapping("/company/delete/{id}")
    public String deleteCompany(@PathVariable(name = "id") int id){
        cmpService.delete(id);
        return "redirect:/company";
    }

    @RequestMapping("/createReportPdfCompany")
    public String exportToPdf() throws FileNotFoundException, JRException{
        cmpService.exportReport("pdf");
        return "redirect:/company";
    }

    @RequestMapping("/createReportXlsCompany")
    public String exportToXls() throws FileNotFoundException, JRException{
        cmpService.exportReport("xls");
        return "redirect:/company";
    }
}
