package com.ichmal.aplikasi_struktur_organisasi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class employee {
    private int id;
    private String nama;
    private String atasanId;
    private String companyId;

    public employee(){
    }

    public employee(int id, String nama, String atasanId, String companyId) {
        this.id = id;
        this.nama = nama;
        this.atasanId = atasanId;
        this.companyId = companyId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAtasanId() {
        return atasanId;
    }

    public void setAtasanId(String atasanId) {
        this.atasanId = atasanId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
