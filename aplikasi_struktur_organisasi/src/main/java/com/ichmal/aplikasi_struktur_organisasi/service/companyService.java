package com.ichmal.aplikasi_struktur_organisasi.service;

import com.ichmal.aplikasi_struktur_organisasi.model.company;
import com.ichmal.aplikasi_struktur_organisasi.repository.companyRepo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class companyService {
    @Autowired
    private companyRepo cmpRepo;

    public List<company> listAll(){
        return cmpRepo.findAll();
    }

    public void save(company company){
        cmpRepo.save(company);
    }

    public company getId(int id){
        return cmpRepo.findById(id).get();
    }

    public void delete(int id){
        cmpRepo.deleteById(id);
    }

    public void exportReport(String reportFormat) throws FileNotFoundException, JRException{
        String pathFilePdf = "File/ReportCompany.pdf";
        String pathFileXls = "File/ReportCompany.xlsx";

        List<company> companyList = listAll();

        File file = ResourceUtils.getFile("classpath:templates/ReportCompany.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(companyList);
        Map<String, Object> params = new HashMap<>();
        params.put("Created By", "Muhammad Ichmal Gumanof");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
        if (reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint, pathFilePdf);
        } else if (reportFormat.equalsIgnoreCase("xls")){
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathFileXls));
            exporter.exportReport();
        }
    }
}
