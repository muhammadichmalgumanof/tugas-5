package com.ichmal.aplikasi_struktur_organisasi.repository;

import com.ichmal.aplikasi_struktur_organisasi.model.employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface employeeRepo extends JpaRepository<employee, Integer> {
    @Query(value = "SELECT e.id, e.nama, if(em.nama=e.nama,null, em.nama), c.nama from employee e join employee em join company c on e.company_id = c.id where e.atasan_id = em.id or e.atasan_id is null group by e.id", nativeQuery = true)
    List<employee> Tampil();
}
